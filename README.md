## How to upgrade Debian 11 (Bullseye) to 12 (Bookworm)

1. Backup the system.
2. Update existing packages and reboot the Debian 11 system.
3. Edit the file `/etc/apt/sources.list` using a text editor and replace each instance of `bullseye` with `bookworm`. Next find the update line, replace keyword `bullseye-updates` with `bookworm-updates`. Finally, search the security line, replace keyword `bullseye-security` with `bookworm-security`.
4. Update the packages index on Debian Linux, run:
`sudo apt update`
5. Prepare for the operating system minimal system upgrade, run:
`sudo apt upgrade --without-new-pkgs`
6. Finally, update Debian 11 to Debian 12 Bookworm by running:
`sudo apt full-upgrade`
7. Reboot the Linux system so that you can boot into Debian 12 Bookworm
Verify that everything is working correctly.